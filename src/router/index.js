// 路由配置文件
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import { checkToken } from '../../api/apis'


Vue.use(VueRouter)



var router = new VueRouter({
  routes: [{
    //登陆一级页面的路由
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    // 登陆进来后的一级页面
    path: '/main',
    name: 'main',
    component: () => import('../views/Main.vue'),
    // 子路由，二级页面
    children: [{
      //主页
      path: "/main/index",
      name: "/main/index",
      component: () => import('../views/main/Index.vue'),
      meta: { breadList: ["首页"] },
    },
    {
      // ---------------------账号管理模块-----------------------------
      //添加账号
      path: "/main/UserAdd",
      name: "/main/UserAdd",
      component: () => import('../views/main/users/UserAdd.vue'),
      meta: { breadList: ["账号管理", "添加账号"] },
    },
    {//修改用户
      path: "/main/UserEdit",
      name: "/main/UserEdit",
      component: () => import('../views/main/users/UserEdit.vue'),
      meta: { breadList: ["账号管理", "修改用户"] },
    },
    {//账号列表
      path: "/main/UserList",
      name: "/main/UserList",
      component: () => import('../views/main/users/UserList.vue'),
      meta: { breadList: ["账号管理", "账号列表"] },
    },
    //  ---------------------商品管理模块-----------------------------
    {
      // 商品列表
      path: "/main/itemlist",
      name: "/main/itemlist",
      component: () => import('../views/main/items/ItemList.vue'),
      meta: { breadList: ["商品管理", "商品列表"] },
    },
    {
      // 添加商品 
      path: "/main/additem",
      name: "/main/additem",
      component: () => import('../views/main/items/AddItem.vue'),
      meta: { breadList: ["商品管理", "添加商品"] },
    },
    {
      // 商品分类
      path: "/main/itemsort",
      name: "/main/itemsort",
      component: () => import('../views/main/items/ItemSort.vue'),
      meta: { breadList: ["商品管理", "商品分类"] },
    },

    //  ---------------------订单管理模块-----------------------------
    {
      // 订单管理
      path: "/main/order",
      name: "/main/order",
      component: () => import('../views/main/order/Order.vue'),
      meta: { breadList: ["订单管理"] },
    },
    //  ---------------------店铺管理模块-----------------------------
    {
      // 店铺管理
      path: '/main/shop',
      name: '/main/shop',
      component: () => import('../views/main/shops/ShopManagement.vue'),
      meta: { breadList: ["店铺管理"] }
    },
    // ---------------------销售统计模块-----------------------------
    {
      //商品统计
      path: "/main/sellTotal",
      name: "/main/sellTotal",
      component: () => import('../views/main/sell/SellTotal.vue'),
      meta: { breadList: ["销售统计", "商品统计"] }
    },

    {
      //订单统计
      path: "/main/orderTotal",
      name: "/main/orderTotal",
      component: () => import('../views/main/sell/OrderTotal.vue'),
      meta: { breadList: ["销售统计", "订单统计"] }
    }

    ]
  }]
})
//路由拦截，全局前置守卫 在切换路由前
// 在切换路由前  to跳转去哪里  from来自哪里(哪个hash地址)  next放行(next是一个函数)
router.beforeEach((to, from, next) => {
  console.log(from)
  // 如果是登陆页面则走next()进行正常的页面跳转。访问不是登陆页面则需要判断用户是否登陆，登陆的话就next(),没登陆的就跳转登陆页面去
  if (to.path != "/") {
    checkToken(localStorage.token).then((res) => {
      if (res.data.code == 0) {
        next() //让它正常跳转
      } else
        next('/')
    })

  } else
    next()

})


export default router;

