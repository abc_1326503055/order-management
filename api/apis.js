// 封装所有api接口，发送所有axios请求
import axios from 'axios'
const IP = "http://127.0.0.1:5000"
// 设置baseURL全局地址
axios.defaults.baseURL = IP;
// 1.商品图片上传接口
export const UPLOAD_IMG = IP + "/goods/goods_img_upload"
// 2.获取自己上传的商品图片
export const GET_UPLOAD_GOODS_IMG = IP + "/upload/imgs/goods_img/"
// 3.用户头像上传接口
export const UPLOAD_HEAD_IMG = IP + "/users/avatar_upload"
// 4.获取用户上传的头像
export const GET_HEAD_IMG = IP + "/upload/imgs/acc_img/"
// 5.获取店铺图片和头像
export const Get_Shop_Img = IP + "/upload/shop/"
// 6.店铺图片和头像上传接口
export const Upload_Shop_Img = IP + "/shop/upload"



// --------------------------一、账号管理模块接口-------------------------------------------------------
//1.登陆接口
// export function login(account, password) {
//     return axios.post("/users/checkLogin", {
//         account:account,
//          password:password
//     })
// }
export var login = (account, password) => axios.post('/users/checkLogin', { account, password })
// 2.验证token是否过期接口
export var checkToken = (token) => axios.get('/users/checktoken', { params: { token } })
// 3.账号添加
export var userAdd = (account, password, userGroup) => axios.post('/users/add', { account, password, userGroup })
// 4.账号列表
export var userList = (currentPage, pageSize) => axios.get('/users/list', { params: { currentPage, pageSize } })
// 5.账号删除
export var userDel = (id) => axios.get('/users/del', { params: { id: id } })
// 6.账号批量删除
export var userBatchDel = (ids) => axios.get('/users/batchdel', { params: { ids } })
// 7.修改账号
export var useredit = (obj) => axios.post('/users/edit', obj)
// 8.获取账号信息（用户头像）
export var userImg = (id) => axios.get('/users/accountinfo', { params: { id } })
// 9.修改密码
export var editpwd = (newPwd, id) => axios.post("/users/editpwd", { newPwd, id })


// --------------------------二、商品管理模块接口-------------------------------------------------------
// 1.获取分类
export var catelist = (currentPage, pageSize) => axios.get('/goods/catelist', { params: { currentPage, pageSize } })
// 2.添加分类[传的参数太多时可以直接传对象过去，那边接的是scope.row]
export var editcate = (obj) => axios.post('/goods/editcate', obj)
// 3.删除分类
export var delcate = (id) => axios.get('/goods/delcate', { params: { id } })
// 4.获取所有分类
export var categories = () => axios.get('/goods/categories')

// 5.添加商品
export var goodsAdd = (params) => axios.post('/goods/add', params)
//6.获取商品列表
export var goodsList = (currentPage, pageSize) => axios.get('/goods/list', { params: { currentPage, pageSize } })
// 7.修改商品
export var goodsEdit = (params) => axios.post('/goods/edit', params)
// 8.删除商品
export var goodsDel = (id) => axios.get('/goods/del', { params: { id } })

// --------------------------三、订单管理模块接口-------------------------------------------------------
export var orderList = (params) => axios.get("/order/list", { params })

// --------------------------四、报表统计模块接口-------------------------------------------------------
//1.首页报表接口
export var OrderTotalData = () => axios.get("/order/totaldata")
// 2.订单报表接口
export var OrderTotal = (date) => axios.get("/order/ordertotal", { params: { date } })

// --------------------------五、店铺管理模块接口-------------------------------------------------------
// 1.获取店铺详情
export var ShopInfo = () => axios.get("/shop/info")
// 2.图片上传接口
export var ShopUpload = () => axios.post("/shop/upload")
// 3.店铺内容修改
export var ShopEdit = (params) => axios.post("/shop/edit", params)

