# 甜品店后台管理系统

## 项目介绍
项目技术栈：Vue.js + vue-router + axios + ElementUI +echarts；父子路由，bus传参，父子传参。
甜品店后台管理系统，所有的数据都是从服务器实时获取的真实数据，具有真实的注册、登录、数据显示、新增数据、修改数据、删除数据等功能，方便商家管理甜品店（APP）外卖业务，根据另外一套连动起来，[查看另外一个网站（甜品店APP）](https://gitee.com/abc_1326503055/dessert-shop-app-order.git)，这项目包含PC端和移动端，两者实现完整的一套外卖点餐系统。

## 使用说明

- 1. 需要提前安装好 nodejs 与 yarn,下载项目后在项目主目录下运行 yarn 拉取依赖包。
- 2.  运行后台服务器，打开sell-serve文件，在控制台输入yarn serve 启动后台数据。
- 3.  启动项目：直接使用命令 yarn serve或npm run serve。
- 4.  **项目初始的登陆账号：admin；密码：admin** 




### 打开网址
复制控制台的网址到浏览器即可查看项目。

### 项目进行打包
yarn build

## Vue-cli搭建开发环境
### 搭建项目架构：
项目采用Webpack+Vue-router的架构方式，开始安装（windows系统上操作）
1. 配置npm的淘宝镜像
    npm config set registry https://registry.npmjs.org/
2. 下载安装配置yarn
-   2.1 npm i yarn -g
-   2.2 配置path环境变量
-       yarn global bin //查看全局安装目录,把目录粘贴到系统环境变量PATH中(增量更新,不要覆盖之前的)
-   2.3 配置yarn淘宝镜像
-       yarn config set registry http://registry.npm.taobao.org/

			
3. 安装vue脚手架,创建项目  
-  3.1 下载脚手架
-     yarn global add @vue/cli
-  3.2 使用脚手架创建项目
-     vue create 项目名(不能中文)
-  3.3 启动运行项目
-     yarn serve



## src下的项目目录结构
| api        | 封装请求接口 |
|------------|--------|
| assets     | 静态资源   |
| components | 公共组件   |
| router     | 路由配置   |
| views      | 页面文件   |
| json       | json文件 |
| App.vue    | 入口视图   |
| main.js    | 入口js文件 |


## 项目建模
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/image.png)

## 项目效果图
### 登陆页面
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E7%99%BB%E9%99%86.PNG)
### 后台外卖商家中心
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E9%A6%96%E9%A1%B5.PNG)

### 订单管理页面
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E8%AE%A2%E5%8D%95%E7%AE%A1%E7%90%86.PNG)
### 商品管理模块
#### 商品列表
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E5%95%86%E5%93%81%E5%88%97%E8%A1%A8.PNG)
#### 添加商品
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E6%B7%BB%E5%8A%A0%E5%95%86%E5%93%81.PNG)
#### 商品分类
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E5%95%86%E5%93%81%E5%88%86%E7%B1%BB.PNG)
### 店铺管理页面
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E5%BA%97%E9%93%BA%E7%AE%A1%E7%90%861.PNG)
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E5%BA%97%E9%93%BA%E7%AE%A1%E7%90%862.PNG)
### 账号管理
#### 账号列表
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E8%B4%A6%E5%8F%B7%E5%88%97%E8%A1%A8.PNG)
#### 添加账号
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E6%B7%BB%E5%8A%A0%E8%B4%A6%E5%8F%B7.PNG)
#### 修改用户
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E4%BF%AE%E6%94%B9%E7%94%A8%E6%88%B7.PNG)

### 销售统计
#### 订单统计
![输入图片说明](%E9%A1%B9%E7%9B%AE%E6%95%88%E6%9E%9C%E9%A2%84%E8%A7%88%E5%9B%BE/%E8%AE%A2%E5%8D%95%E7%BB%9F%E8%AE%A1.PNG)


## **打包之前的路由模式**

**`目标`**配置打包之前的路由模式

> 在SPA单页应用中，有两种路由模式

**hash模式** ： #后面是路由路径，特点是前端访问，#后面的变化不会经过服务器

**history模式**：正常的/访问模式，特点是后端访问，任意地址的变化都会访问服务器

> 开发到现在，我们一直都在用hash模式，打包我们尝试用history模式

改成history模式非常简单，只需要将路由的mode类型改成history即可

```js
const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }), // 管理滚动行为 如果出现滚动 切换就让 让页面回到顶部
  routes: [...constantRoutes] // 改成只有静态路由
})
```



我们会发现，其实域名是**`www.xxxx/com`**，hr是特定的前缀地址，此时我们可以配置一个base属性，配置为hr

```js
const createRouter = () => new Router({
  mode: 'history', // require service support
  base: '/hr/', // 配置项目的基础地址
  scrollBehavior: () => ({ y: 0 }), // 管理滚动行为 如果出现滚动 切换就让 让页面回到顶部
  routes: [...constantRoutes] // 改成只有静态路由
})
```

此时，我们会发现地址已经变成我们想要的样子了

![image-20200804014626686](assets/image-20200804014626686.png)

**提交代码**

## 性能分析和CDN的应用

**`目标`**： 对开发的应用进行性能分析和CDN的应用

### 性能分析

> 我们集成了 功能，写了很多组件，最终都会打包成一堆文件，那么真实运行的性能如何呢？

我们可以使用vue-cli本身提供的性能分析工具，对我们开发的所有功能进行打包分析

它的应用非常简单

```bash
$ cnpm run preview -- --report
```

这个命令会从我们的**`入口main.js`**进行依赖分析，分析出最大的包，方便我们进行观察和优化

执行完这个命令，我们会看到如下的页面

![image-20200804015849396](assets/image-20200804015849396.png)

如图所以，方块越大，说明该文件占用的文件越大，文件越大，对于网络带宽和访问速度的要求就越高，这也就是我们优化的方向

> 像这种情况，我们怎么优化一下呢

### webpack排除打包

CDN是一个比较好的方式

> 文件不是大吗？我们就不要把这些大的文件和那些小的文件打包到一起了，像这种xlsx,element这种功能性很全的插件，我们可以放到CDN服务器上，一来，减轻整体包的大小，二来CDN的加速服务可以加快我们对于插件的访问速度

**使用方式**

先找到 `vue.config.js`， 添加 `externals` 让 `webpack` 不打包 `xlsx` 和 `element`   

**`vue.config.js`**

```js
 // 排除 elementUI xlsx  和 vue 
  externals:
      {
        'vue': 'Vue',
        'element-ui': 'ELEMENT',
        'xlsx': 'XLSX'
     }
```

再次运行，我们会发现包的大小已经大幅减小

### CDN文件配置

> 但是，没有被打包的几个模块怎么处理？

可以采用CDN的方式，在页面模板中预先引入

**`vue.config.js`**

```js
const cdn = {
  css: [
    // element-ui css
    'https://unpkg.com/element-ui/lib/theme-chalk/index.css' // 样式表
  ],
  js: [
    // vue must at first!
    'https://unpkg.com/vue/dist/vue.js', // vuejs
    // element-ui js
    'https://unpkg.com/element-ui/lib/index.js', // elementUI
      'https://cdn.jsdelivr.net/npm/xlsx@0.16.6/dist/jszip.min.js',
    'https://cdn.jsdelivr.net/npm/xlsx@0.16.6/dist/xlsx.full.min.js'
  ]
}
```

> 但是请注意，这时的配置实际上是对开发环境和生产环境都生效的，在开发环境时，没有必要使用CDN，此时我们可以使用环境变量来进行区分

```js
let cdn = { css: [], js: [] }
// 通过环境变量 来区分是否使用cdn
const isProd = process.env.NODE_ENV === 'production' // 判断是否是生产环境
let externals = {}
if (isProd) {
  // 如果是生产环境 就排除打包 否则不排除
  externals = {
    // key(包名) / value(这个值 是 需要在CDN中获取js, 相当于 获取的js中 的该包的全局的对象的名字)
    'vue': 'Vue', // 后面的名字不能随便起 应该是 js中的全局对象名
    'element-ui': 'ELEMENT', // 都是js中全局定义的
    'xlsx': 'XLSX' // 都是js中全局定义的
  }
  cdn = {
    css: [
      'https://unpkg.com/element-ui/lib/theme-chalk/index.css' // 提前引入elementUI样式
    ], // 放置css文件目录
    js: [
      'https://unpkg.com/vue/dist/vue.js', // vuejs
      'https://unpkg.com/element-ui/lib/index.js', // element
      'https://cdn.jsdelivr.net/npm/xlsx@0.16.6/dist/xlsx.full.min.js', // xlsx 相关
      'https://cdn.jsdelivr.net/npm/xlsx@0.16.6/dist/jszip.min.js' // xlsx 相关
    ] // 放置js文件目录
  }
}
```

### 注入CDN文件到模板

之后通过 `html-webpack-plugin`注入到 `index.html`之中:

```js
config.plugin('html').tap(args => {
  args[0].cdn = cdn
  return args
})
```

找到 `public/index.html`。通过你配置的`CDN Config` 依次注入 css 和 js。

```vue
<head>
  <!-- 引入样式 -->
  <% for(var css of htmlWebpackPlugin.options.cdn.css) { %>
    <link rel="stylesheet" href="<%=css%>">
  <% } %>
</head>

<!-- 引入JS -->
<% for(var js of htmlWebpackPlugin.options.cdn.js) { %>
  <script src="<%=js%>"></script>
<% } %>
```

最后，进行打包 


 yarn build







